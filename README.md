# C Linear Algebra Library

## Setup
You just need to copy the header clal.h and the .so libclal.so into your project and include it like so:
```
#include "clal.h"
```
then you can compile yout program like:
```
$ gcc -L /path/to/.so -Wl,-rpath=/path/to/.so your_program.c -lclal
```
if you want an easier time compiling you can just
```
# cp libclal.so /usr/lib && cp clal.h /usr/include
```
and then include the header like
```
#include <clal.h>
```
and compile with
```
gcc your_program.c -lclal
```

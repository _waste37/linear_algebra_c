#include <stdio.h>
#include "clal.h"
/* compiled in this folder with
 * gcc -L . -Wl,-rpath=. example.c -lclal 
 * */
int main(void)
{
    V3i v = v3is(2), s = v3i(1,2,3);
    V3i r = v3i_sum(v, s);
    printf(V3i_fmt" + "V3i_fmt" = "V3i_fmt"\n", V3i_arg(v), V3i_arg(s), V3i_arg(r));
    return 0;
}

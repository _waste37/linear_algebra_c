#ifndef CLAL_H_
#define CLAL_H_

#include <stdlib.h>
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
typedef struct { float c[2]; } V2f;
typedef struct { double c[2]; } V2d;
typedef struct { int c[2]; } V2i;
typedef struct { unsigned int c[2]; } V2u;
typedef struct { size_t c[2]; } V2s;
typedef struct { float c[3]; } V3f;
typedef struct { double c[3]; } V3d;
typedef struct { int c[3]; } V3i;
typedef struct { unsigned int c[3]; } V3u;
typedef struct { size_t c[3]; } V3s;
typedef struct { float c[4]; } V4f;
typedef struct { double c[4]; } V4d;
typedef struct { int c[4]; } V4i;
typedef struct { unsigned int c[4]; } V4u;
typedef struct { size_t c[4]; } V4s;

typedef struct { float c[2][2]; } M2f;
typedef struct { double c[2][2]; } M2d;
typedef struct { int c[2][2]; } M2i;
typedef struct { unsigned int c[2][2]; } M2u;
typedef struct { size_t c[2][2]; } M2s;
typedef struct { float c[2][3]; } M2x3f;
typedef struct { double c[2][3]; } M2x3d;
typedef struct { int c[2][3]; } M2x3i;
typedef struct { unsigned int c[2][3]; } M2x3u;
typedef struct { size_t c[2][3]; } M2x3s;
typedef struct { float c[2][4]; } M2x4f;
typedef struct { double c[2][4]; } M2x4d;
typedef struct { int c[2][4]; } M2x4i;
typedef struct { unsigned int c[2][4]; } M2x4u;
typedef struct { size_t c[2][4]; } M2x4s;
typedef struct { float c[3][2]; } M3x2f;
typedef struct { double c[3][2]; } M3x2d;
typedef struct { int c[3][2]; } M3x2i;
typedef struct { unsigned int c[3][2]; } M3x2u;
typedef struct { size_t c[3][2]; } M3x2s;
typedef struct { float c[3][3]; } M3f;
typedef struct { double c[3][3]; } M3d;
typedef struct { int c[3][3]; } M3i;
typedef struct { unsigned int c[3][3]; } M3u;
typedef struct { size_t c[3][3]; } M3s;
typedef struct { float c[3][4]; } M3x4f;
typedef struct { double c[3][4]; } M3x4d;
typedef struct { int c[3][4]; } M3x4i;
typedef struct { unsigned int c[3][4]; } M3x4u;
typedef struct { size_t c[3][4]; } M3x4s;
typedef struct { float c[4][2]; } M4x2f;
typedef struct { double c[4][2]; } M4x2d;
typedef struct { int c[4][2]; } M4x2i;
typedef struct { unsigned int c[4][2]; } M4x2u;
typedef struct { size_t c[4][2]; } M4x2s;
typedef struct { float c[4][3]; } M4x3f;
typedef struct { double c[4][3]; } M4x3d;
typedef struct { int c[4][3]; } M4x3i;
typedef struct { unsigned int c[4][3]; } M4x3u;
typedef struct { size_t c[4][3]; } M4x3s;
typedef struct { float c[4][4]; } M4f;
typedef struct { double c[4][4]; } M4d;
typedef struct { int c[4][4]; } M4i;
typedef struct { unsigned int c[4][4]; } M4u;
typedef struct { size_t c[4][4]; } M4s;

#define V2f_fmt "v2f(%f, %f)"
#define V2f_arg(x) x.c[0], x.c[1]

V2f v2f(float x0, float x1);
V2f v2fs(float x);
V2f v2f_sum(V2f x, V2f y);
V2f v2f_sub(V2f x, V2f y);
V2f v2f_smulf(float s, V2f v);
V2f v2f_smuld(double s, V2f v);
V2f v2f_smuli(int s, V2f v);
V2f v2f_smulu(unsigned int s, V2f v);
V2f v2f_smuls(size_t s, V2f v);
float v2f_scalar(V2f x, V2f y);
float v2f_norm(V2f v);

#define V2d_fmt "v2d(%f, %f)"
#define V2d_arg(x) x.c[0], x.c[1]

V2d v2d(double x0, double x1);
V2d v2ds(double x);
V2d v2d_sum(V2d x, V2d y);
V2d v2d_sub(V2d x, V2d y);
V2d v2d_smulf(float s, V2d v);
V2d v2d_smuld(double s, V2d v);
V2d v2d_smuli(int s, V2d v);
V2d v2d_smulu(unsigned int s, V2d v);
V2d v2d_smuls(size_t s, V2d v);
double v2d_scalar(V2d x, V2d y);
double v2d_norm(V2d v);

#define V2i_fmt "v2i(%d, %d)"
#define V2i_arg(x) x.c[0], x.c[1]

V2i v2i(int x0, int x1);
V2i v2is(int x);
V2i v2i_sum(V2i x, V2i y);
V2i v2i_sub(V2i x, V2i y);
V2i v2i_smulf(float s, V2i v);
V2i v2i_smuld(double s, V2i v);
V2i v2i_smuli(int s, V2i v);
V2i v2i_smulu(unsigned int s, V2i v);
V2i v2i_smuls(size_t s, V2i v);
int v2i_scalar(V2i x, V2i y);
float v2i_norm(V2i v);

#define V2u_fmt "v2u(%u, %u)"
#define V2u_arg(x) x.c[0], x.c[1]

V2u v2u(unsigned int x0, unsigned int x1);
V2u v2us(unsigned int x);
V2u v2u_sum(V2u x, V2u y);
V2u v2u_sub(V2u x, V2u y);
V2u v2u_smulf(float s, V2u v);
V2u v2u_smuld(double s, V2u v);
V2u v2u_smuli(int s, V2u v);
V2u v2u_smulu(unsigned int s, V2u v);
V2u v2u_smuls(size_t s, V2u v);
unsigned int v2u_scalar(V2u x, V2u y);
float v2u_norm(V2u v);

#define V2s_fmt "v2s(%zu, %zu)"
#define V2s_arg(x) x.c[0], x.c[1]

V2s v2s(size_t x0, size_t x1);
V2s v2ss(size_t x);
V2s v2s_sum(V2s x, V2s y);
V2s v2s_sub(V2s x, V2s y);
V2s v2s_smulf(float s, V2s v);
V2s v2s_smuld(double s, V2s v);
V2s v2s_smuli(int s, V2s v);
V2s v2s_smulu(unsigned int s, V2s v);
V2s v2s_smuls(size_t s, V2s v);
size_t v2s_scalar(V2s x, V2s y);
float v2s_norm(V2s v);


#define V3f_fmt "v3f(%f, %f, %f)"
#define V3f_arg(x) x.c[0], x.c[1], x.c[2]

V3f v3f(float x0, float x1, float x2);
V3f v3fs(float x);
V3f v3f_sum(V3f x, V3f y);
V3f v3f_sub(V3f x, V3f y);
V3f v3f_smulf(float s, V3f v);
V3f v3f_smuld(double s, V3f v);
V3f v3f_smuli(int s, V3f v);
V3f v3f_smulu(unsigned int s, V3f v);
V3f v3f_smuls(size_t s, V3f v);
float v3f_scalar(V3f x, V3f y);
float v3f_norm(V3f v);

#define V3d_fmt "v3d(%f, %f, %f)"
#define V3d_arg(x) x.c[0], x.c[1], x.c[2]

V3d v3d(double x0, double x1, double x2);
V3d v3ds(double x);
V3d v3d_sum(V3d x, V3d y);
V3d v3d_sub(V3d x, V3d y);
V3d v3d_smulf(float s, V3d v);
V3d v3d_smuld(double s, V3d v);
V3d v3d_smuli(int s, V3d v);
V3d v3d_smulu(unsigned int s, V3d v);
V3d v3d_smuls(size_t s, V3d v);
double v3d_scalar(V3d x, V3d y);
double v3d_norm(V3d v);

#define V3i_fmt "v3i(%d, %d, %d)"
#define V3i_arg(x) x.c[0], x.c[1], x.c[2]

V3i v3i(int x0, int x1, int x2);
V3i v3is(int x);
V3i v3i_sum(V3i x, V3i y);
V3i v3i_sub(V3i x, V3i y);
V3i v3i_smulf(float s, V3i v);
V3i v3i_smuld(double s, V3i v);
V3i v3i_smuli(int s, V3i v);
V3i v3i_smulu(unsigned int s, V3i v);
V3i v3i_smuls(size_t s, V3i v);
int v3i_scalar(V3i x, V3i y);
float v3i_norm(V3i v);

#define V3u_fmt "v3u(%u, %u, %u)"
#define V3u_arg(x) x.c[0], x.c[1], x.c[2]

V3u v3u(unsigned int x0, unsigned int x1, unsigned int x2);
V3u v3us(unsigned int x);
V3u v3u_sum(V3u x, V3u y);
V3u v3u_sub(V3u x, V3u y);
V3u v3u_smulf(float s, V3u v);
V3u v3u_smuld(double s, V3u v);
V3u v3u_smuli(int s, V3u v);
V3u v3u_smulu(unsigned int s, V3u v);
V3u v3u_smuls(size_t s, V3u v);
unsigned int v3u_scalar(V3u x, V3u y);
float v3u_norm(V3u v);

#define V3s_fmt "v3s(%zu, %zu, %zu)"
#define V3s_arg(x) x.c[0], x.c[1], x.c[2]

V3s v3s(size_t x0, size_t x1, size_t x2);
V3s v3ss(size_t x);
V3s v3s_sum(V3s x, V3s y);
V3s v3s_sub(V3s x, V3s y);
V3s v3s_smulf(float s, V3s v);
V3s v3s_smuld(double s, V3s v);
V3s v3s_smuli(int s, V3s v);
V3s v3s_smulu(unsigned int s, V3s v);
V3s v3s_smuls(size_t s, V3s v);
size_t v3s_scalar(V3s x, V3s y);
float v3s_norm(V3s v);


#define V4f_fmt "v4f(%f, %f, %f, %f)"
#define V4f_arg(x) x.c[0], x.c[1], x.c[2], x.c[3]

V4f v4f(float x0, float x1, float x2, float x3);
V4f v4fs(float x);
V4f v4f_sum(V4f x, V4f y);
V4f v4f_sub(V4f x, V4f y);
V4f v4f_smulf(float s, V4f v);
V4f v4f_smuld(double s, V4f v);
V4f v4f_smuli(int s, V4f v);
V4f v4f_smulu(unsigned int s, V4f v);
V4f v4f_smuls(size_t s, V4f v);
float v4f_scalar(V4f x, V4f y);
float v4f_norm(V4f v);

#define V4d_fmt "v4d(%f, %f, %f, %f)"
#define V4d_arg(x) x.c[0], x.c[1], x.c[2], x.c[3]

V4d v4d(double x0, double x1, double x2, double x3);
V4d v4ds(double x);
V4d v4d_sum(V4d x, V4d y);
V4d v4d_sub(V4d x, V4d y);
V4d v4d_smulf(float s, V4d v);
V4d v4d_smuld(double s, V4d v);
V4d v4d_smuli(int s, V4d v);
V4d v4d_smulu(unsigned int s, V4d v);
V4d v4d_smuls(size_t s, V4d v);
double v4d_scalar(V4d x, V4d y);
double v4d_norm(V4d v);

#define V4i_fmt "v4i(%d, %d, %d, %d)"
#define V4i_arg(x) x.c[0], x.c[1], x.c[2], x.c[3]

V4i v4i(int x0, int x1, int x2, int x3);
V4i v4is(int x);
V4i v4i_sum(V4i x, V4i y);
V4i v4i_sub(V4i x, V4i y);
V4i v4i_smulf(float s, V4i v);
V4i v4i_smuld(double s, V4i v);
V4i v4i_smuli(int s, V4i v);
V4i v4i_smulu(unsigned int s, V4i v);
V4i v4i_smuls(size_t s, V4i v);
int v4i_scalar(V4i x, V4i y);
float v4i_norm(V4i v);

#define V4u_fmt "v4u(%u, %u, %u, %u)"
#define V4u_arg(x) x.c[0], x.c[1], x.c[2], x.c[3]

V4u v4u(unsigned int x0, unsigned int x1, unsigned int x2, unsigned int x3);
V4u v4us(unsigned int x);
V4u v4u_sum(V4u x, V4u y);
V4u v4u_sub(V4u x, V4u y);
V4u v4u_smulf(float s, V4u v);
V4u v4u_smuld(double s, V4u v);
V4u v4u_smuli(int s, V4u v);
V4u v4u_smulu(unsigned int s, V4u v);
V4u v4u_smuls(size_t s, V4u v);
unsigned int v4u_scalar(V4u x, V4u y);
float v4u_norm(V4u v);

#define V4s_fmt "v4s(%zu, %zu, %zu, %zu)"
#define V4s_arg(x) x.c[0], x.c[1], x.c[2], x.c[3]

V4s v4s(size_t x0, size_t x1, size_t x2, size_t x3);
V4s v4ss(size_t x);
V4s v4s_sum(V4s x, V4s y);
V4s v4s_sub(V4s x, V4s y);
V4s v4s_smulf(float s, V4s v);
V4s v4s_smuld(double s, V4s v);
V4s v4s_smuli(int s, V4s v);
V4s v4s_smulu(unsigned int s, V4s v);
V4s v4s_smuls(size_t s, V4s v);
size_t v4s_scalar(V4s x, V4s y);
float v4s_norm(V4s v);


M2f m2f(float x00, float x01, float x10, float x11);
M2f m2fs(float x);
M2f m2fc(V2f x0, V2f x1);
M2f m2fr(V2f x0, V2f x1);
M2f m2f_transpose(M2f m);
M2f m2f_sum(M2f x, M2f y);
M2f m2f_sub(M2f x, M2f y);
M2f m2f_smulf(float s, M2f m);
M2f m2f_smuld(double s, M2f m);
M2f m2f_smuli(int s, M2f m);
M2f m2f_smulu(unsigned int s, M2f m);
M2f m2f_smuls(size_t s, M2f m);
V2f m2f_vmul(M2f m, V2f v);
M2f m2f_mul2x2(M2f m, M2f n);

M2d m2d(double x00, double x01, double x10, double x11);
M2d m2ds(double x);
M2d m2dc(V2d x0, V2d x1);
M2d m2dr(V2d x0, V2d x1);
M2d m2d_transpose(M2d m);
M2d m2d_sum(M2d x, M2d y);
M2d m2d_sub(M2d x, M2d y);
M2d m2d_smulf(float s, M2d m);
M2d m2d_smuld(double s, M2d m);
M2d m2d_smuli(int s, M2d m);
M2d m2d_smulu(unsigned int s, M2d m);
M2d m2d_smuls(size_t s, M2d m);
V2d m2d_vmul(M2d m, V2d v);
M2d m2d_mul2x2(M2d m, M2d n);

M2i m2i(int x00, int x01, int x10, int x11);
M2i m2is(int x);
M2i m2ic(V2i x0, V2i x1);
M2i m2ir(V2i x0, V2i x1);
M2i m2i_transpose(M2i m);
M2i m2i_sum(M2i x, M2i y);
M2i m2i_sub(M2i x, M2i y);
M2i m2i_smulf(float s, M2i m);
M2i m2i_smuld(double s, M2i m);
M2i m2i_smuli(int s, M2i m);
M2i m2i_smulu(unsigned int s, M2i m);
M2i m2i_smuls(size_t s, M2i m);
V2i m2i_vmul(M2i m, V2i v);
M2i m2i_mul2x2(M2i m, M2i n);

M2u m2u(unsigned int x00, unsigned int x01, unsigned int x10, unsigned int x11);
M2u m2us(unsigned int x);
M2u m2uc(V2u x0, V2u x1);
M2u m2ur(V2u x0, V2u x1);
M2u m2u_transpose(M2u m);
M2u m2u_sum(M2u x, M2u y);
M2u m2u_sub(M2u x, M2u y);
M2u m2u_smulf(float s, M2u m);
M2u m2u_smuld(double s, M2u m);
M2u m2u_smuli(int s, M2u m);
M2u m2u_smulu(unsigned int s, M2u m);
M2u m2u_smuls(size_t s, M2u m);
V2u m2u_vmul(M2u m, V2u v);
M2u m2u_mul2x2(M2u m, M2u n);

M2s m2s(size_t x00, size_t x01, size_t x10, size_t x11);
M2s m2ss(size_t x);
M2s m2sc(V2s x0, V2s x1);
M2s m2sr(V2s x0, V2s x1);
M2s m2s_transpose(M2s m);
M2s m2s_sum(M2s x, M2s y);
M2s m2s_sub(M2s x, M2s y);
M2s m2s_smulf(float s, M2s m);
M2s m2s_smuld(double s, M2s m);
M2s m2s_smuli(int s, M2s m);
M2s m2s_smulu(unsigned int s, M2s m);
M2s m2s_smuls(size_t s, M2s m);
V2s m2s_vmul(M2s m, V2s v);
M2s m2s_mul2x2(M2s m, M2s n);

M2x3f m2x3f(float x00, float x01, float x02, float x10, float x11, float x12);
M2x3f m2x3fs(float x);
M2x3f m2x3fc(V3f x0, V3f x1, V3f x2);
M2x3f m2x3fr(V2f x0, V2f x1);
M3x2f m2x3f_transpose(M2x3f m);
M2x3f m2x3f_sum(M2x3f x, M2x3f y);
M2x3f m2x3f_sub(M2x3f x, M2x3f y);
M2x3f m2x3f_smulf(float s, M2x3f m);
M2x3f m2x3f_smuld(double s, M2x3f m);
M2x3f m2x3f_smuli(int s, M2x3f m);
M2x3f m2x3f_smulu(unsigned int s, M2x3f m);
M2x3f m2x3f_smuls(size_t s, M2x3f m);
V2f m2x3f_vmul(M2x3f m, V3f v);
M2f m2x3f_mul3x2(M2x3f m, M3x2f n);
M2x3f m2x3f_mul3x3(M2x3f m, M3f n);

M2x3d m2x3d(double x00, double x01, double x02, double x10, double x11, double x12);
M2x3d m2x3ds(double x);
M2x3d m2x3dc(V3d x0, V3d x1, V3d x2);
M2x3d m2x3dr(V2d x0, V2d x1);
M3x2d m2x3d_transpose(M2x3d m);
M2x3d m2x3d_sum(M2x3d x, M2x3d y);
M2x3d m2x3d_sub(M2x3d x, M2x3d y);
M2x3d m2x3d_smulf(float s, M2x3d m);
M2x3d m2x3d_smuld(double s, M2x3d m);
M2x3d m2x3d_smuli(int s, M2x3d m);
M2x3d m2x3d_smulu(unsigned int s, M2x3d m);
M2x3d m2x3d_smuls(size_t s, M2x3d m);
V2d m2x3d_vmul(M2x3d m, V3d v);
M2d m2x3d_mul3x2(M2x3d m, M3x2d n);
M2x3d m2x3d_mul3x3(M2x3d m, M3d n);

M2x3i m2x3i(int x00, int x01, int x02, int x10, int x11, int x12);
M2x3i m2x3is(int x);
M2x3i m2x3ic(V3i x0, V3i x1, V3i x2);
M2x3i m2x3ir(V2i x0, V2i x1);
M3x2i m2x3i_transpose(M2x3i m);
M2x3i m2x3i_sum(M2x3i x, M2x3i y);
M2x3i m2x3i_sub(M2x3i x, M2x3i y);
M2x3i m2x3i_smulf(float s, M2x3i m);
M2x3i m2x3i_smuld(double s, M2x3i m);
M2x3i m2x3i_smuli(int s, M2x3i m);
M2x3i m2x3i_smulu(unsigned int s, M2x3i m);
M2x3i m2x3i_smuls(size_t s, M2x3i m);
V2i m2x3i_vmul(M2x3i m, V3i v);
M2i m2x3i_mul3x2(M2x3i m, M3x2i n);
M2x3i m2x3i_mul3x3(M2x3i m, M3i n);

M2x3u m2x3u(unsigned int x00, unsigned int x01, unsigned int x02, unsigned int x10, unsigned int x11, unsigned int x12);
M2x3u m2x3us(unsigned int x);
M2x3u m2x3uc(V3u x0, V3u x1, V3u x2);
M2x3u m2x3ur(V2u x0, V2u x1);
M3x2u m2x3u_transpose(M2x3u m);
M2x3u m2x3u_sum(M2x3u x, M2x3u y);
M2x3u m2x3u_sub(M2x3u x, M2x3u y);
M2x3u m2x3u_smulf(float s, M2x3u m);
M2x3u m2x3u_smuld(double s, M2x3u m);
M2x3u m2x3u_smuli(int s, M2x3u m);
M2x3u m2x3u_smulu(unsigned int s, M2x3u m);
M2x3u m2x3u_smuls(size_t s, M2x3u m);
V2u m2x3u_vmul(M2x3u m, V3u v);
M2u m2x3u_mul3x2(M2x3u m, M3x2u n);
M2x3u m2x3u_mul3x3(M2x3u m, M3u n);

M2x3s m2x3s(size_t x00, size_t x01, size_t x02, size_t x10, size_t x11, size_t x12);
M2x3s m2x3ss(size_t x);
M2x3s m2x3sc(V3s x0, V3s x1, V3s x2);
M2x3s m2x3sr(V2s x0, V2s x1);
M3x2s m2x3s_transpose(M2x3s m);
M2x3s m2x3s_sum(M2x3s x, M2x3s y);
M2x3s m2x3s_sub(M2x3s x, M2x3s y);
M2x3s m2x3s_smulf(float s, M2x3s m);
M2x3s m2x3s_smuld(double s, M2x3s m);
M2x3s m2x3s_smuli(int s, M2x3s m);
M2x3s m2x3s_smulu(unsigned int s, M2x3s m);
M2x3s m2x3s_smuls(size_t s, M2x3s m);
V2s m2x3s_vmul(M2x3s m, V3s v);
M2s m2x3s_mul3x2(M2x3s m, M3x2s n);
M2x3s m2x3s_mul3x3(M2x3s m, M3s n);

M2x4f m2x4f(float x00, float x01, float x02, float x03, float x10, float x11, float x12, float x13);
M2x4f m2x4fs(float x);
M2x4f m2x4fc(V4f x0, V4f x1, V4f x2, V4f x3);
M2x4f m2x4fr(V2f x0, V2f x1);
M4x2f m2x4f_transpose(M2x4f m);
M2x4f m2x4f_sum(M2x4f x, M2x4f y);
M2x4f m2x4f_sub(M2x4f x, M2x4f y);
M2x4f m2x4f_smulf(float s, M2x4f m);
M2x4f m2x4f_smuld(double s, M2x4f m);
M2x4f m2x4f_smuli(int s, M2x4f m);
M2x4f m2x4f_smulu(unsigned int s, M2x4f m);
M2x4f m2x4f_smuls(size_t s, M2x4f m);
V2f m2x4f_vmul(M2x4f m, V4f v);
M2f m2x4f_mul4x2(M2x4f m, M4x2f n);
M2x3f m2x4f_mul4x3(M2x4f m, M4x3f n);
M2x4f m2x4f_mul4x4(M2x4f m, M4f n);

M2x4d m2x4d(double x00, double x01, double x02, double x03, double x10, double x11, double x12, double x13);
M2x4d m2x4ds(double x);
M2x4d m2x4dc(V4d x0, V4d x1, V4d x2, V4d x3);
M2x4d m2x4dr(V2d x0, V2d x1);
M4x2d m2x4d_transpose(M2x4d m);
M2x4d m2x4d_sum(M2x4d x, M2x4d y);
M2x4d m2x4d_sub(M2x4d x, M2x4d y);
M2x4d m2x4d_smulf(float s, M2x4d m);
M2x4d m2x4d_smuld(double s, M2x4d m);
M2x4d m2x4d_smuli(int s, M2x4d m);
M2x4d m2x4d_smulu(unsigned int s, M2x4d m);
M2x4d m2x4d_smuls(size_t s, M2x4d m);
V2d m2x4d_vmul(M2x4d m, V4d v);
M2d m2x4d_mul4x2(M2x4d m, M4x2d n);
M2x3d m2x4d_mul4x3(M2x4d m, M4x3d n);
M2x4d m2x4d_mul4x4(M2x4d m, M4d n);

M2x4i m2x4i(int x00, int x01, int x02, int x03, int x10, int x11, int x12, int x13);
M2x4i m2x4is(int x);
M2x4i m2x4ic(V4i x0, V4i x1, V4i x2, V4i x3);
M2x4i m2x4ir(V2i x0, V2i x1);
M4x2i m2x4i_transpose(M2x4i m);
M2x4i m2x4i_sum(M2x4i x, M2x4i y);
M2x4i m2x4i_sub(M2x4i x, M2x4i y);
M2x4i m2x4i_smulf(float s, M2x4i m);
M2x4i m2x4i_smuld(double s, M2x4i m);
M2x4i m2x4i_smuli(int s, M2x4i m);
M2x4i m2x4i_smulu(unsigned int s, M2x4i m);
M2x4i m2x4i_smuls(size_t s, M2x4i m);
V2i m2x4i_vmul(M2x4i m, V4i v);
M2i m2x4i_mul4x2(M2x4i m, M4x2i n);
M2x3i m2x4i_mul4x3(M2x4i m, M4x3i n);
M2x4i m2x4i_mul4x4(M2x4i m, M4i n);

M2x4u m2x4u(unsigned int x00, unsigned int x01, unsigned int x02, unsigned int x03, unsigned int x10, unsigned int x11, unsigned int x12, unsigned int x13);
M2x4u m2x4us(unsigned int x);
M2x4u m2x4uc(V4u x0, V4u x1, V4u x2, V4u x3);
M2x4u m2x4ur(V2u x0, V2u x1);
M4x2u m2x4u_transpose(M2x4u m);
M2x4u m2x4u_sum(M2x4u x, M2x4u y);
M2x4u m2x4u_sub(M2x4u x, M2x4u y);
M2x4u m2x4u_smulf(float s, M2x4u m);
M2x4u m2x4u_smuld(double s, M2x4u m);
M2x4u m2x4u_smuli(int s, M2x4u m);
M2x4u m2x4u_smulu(unsigned int s, M2x4u m);
M2x4u m2x4u_smuls(size_t s, M2x4u m);
V2u m2x4u_vmul(M2x4u m, V4u v);
M2u m2x4u_mul4x2(M2x4u m, M4x2u n);
M2x3u m2x4u_mul4x3(M2x4u m, M4x3u n);
M2x4u m2x4u_mul4x4(M2x4u m, M4u n);

M2x4s m2x4s(size_t x00, size_t x01, size_t x02, size_t x03, size_t x10, size_t x11, size_t x12, size_t x13);
M2x4s m2x4ss(size_t x);
M2x4s m2x4sc(V4s x0, V4s x1, V4s x2, V4s x3);
M2x4s m2x4sr(V2s x0, V2s x1);
M4x2s m2x4s_transpose(M2x4s m);
M2x4s m2x4s_sum(M2x4s x, M2x4s y);
M2x4s m2x4s_sub(M2x4s x, M2x4s y);
M2x4s m2x4s_smulf(float s, M2x4s m);
M2x4s m2x4s_smuld(double s, M2x4s m);
M2x4s m2x4s_smuli(int s, M2x4s m);
M2x4s m2x4s_smulu(unsigned int s, M2x4s m);
M2x4s m2x4s_smuls(size_t s, M2x4s m);
V2s m2x4s_vmul(M2x4s m, V4s v);
M2s m2x4s_mul4x2(M2x4s m, M4x2s n);
M2x3s m2x4s_mul4x3(M2x4s m, M4x3s n);
M2x4s m2x4s_mul4x4(M2x4s m, M4s n);

M3x2f m3x2f(float x00, float x01, float x10, float x11, float x20, float x21);
M3x2f m3x2fs(float x);
M3x2f m3x2fc(V2f x0, V2f x1);
M3x2f m3x2fr(V3f x0, V3f x1, V3f x2);
M2x3f m3x2f_transpose(M3x2f m);
M3x2f m3x2f_sum(M3x2f x, M3x2f y);
M3x2f m3x2f_sub(M3x2f x, M3x2f y);
M3x2f m3x2f_smulf(float s, M3x2f m);
M3x2f m3x2f_smuld(double s, M3x2f m);
M3x2f m3x2f_smuli(int s, M3x2f m);
M3x2f m3x2f_smulu(unsigned int s, M3x2f m);
M3x2f m3x2f_smuls(size_t s, M3x2f m);
V3f m3x2f_vmul(M3x2f m, V2f v);
M3x2f m3x2f_mul2x2(M3x2f m, M2f n);

M3x2d m3x2d(double x00, double x01, double x10, double x11, double x20, double x21);
M3x2d m3x2ds(double x);
M3x2d m3x2dc(V2d x0, V2d x1);
M3x2d m3x2dr(V3d x0, V3d x1, V3d x2);
M2x3d m3x2d_transpose(M3x2d m);
M3x2d m3x2d_sum(M3x2d x, M3x2d y);
M3x2d m3x2d_sub(M3x2d x, M3x2d y);
M3x2d m3x2d_smulf(float s, M3x2d m);
M3x2d m3x2d_smuld(double s, M3x2d m);
M3x2d m3x2d_smuli(int s, M3x2d m);
M3x2d m3x2d_smulu(unsigned int s, M3x2d m);
M3x2d m3x2d_smuls(size_t s, M3x2d m);
V3d m3x2d_vmul(M3x2d m, V2d v);
M3x2d m3x2d_mul2x2(M3x2d m, M2d n);

M3x2i m3x2i(int x00, int x01, int x10, int x11, int x20, int x21);
M3x2i m3x2is(int x);
M3x2i m3x2ic(V2i x0, V2i x1);
M3x2i m3x2ir(V3i x0, V3i x1, V3i x2);
M2x3i m3x2i_transpose(M3x2i m);
M3x2i m3x2i_sum(M3x2i x, M3x2i y);
M3x2i m3x2i_sub(M3x2i x, M3x2i y);
M3x2i m3x2i_smulf(float s, M3x2i m);
M3x2i m3x2i_smuld(double s, M3x2i m);
M3x2i m3x2i_smuli(int s, M3x2i m);
M3x2i m3x2i_smulu(unsigned int s, M3x2i m);
M3x2i m3x2i_smuls(size_t s, M3x2i m);
V3i m3x2i_vmul(M3x2i m, V2i v);
M3x2i m3x2i_mul2x2(M3x2i m, M2i n);

M3x2u m3x2u(unsigned int x00, unsigned int x01, unsigned int x10, unsigned int x11, unsigned int x20, unsigned int x21);
M3x2u m3x2us(unsigned int x);
M3x2u m3x2uc(V2u x0, V2u x1);
M3x2u m3x2ur(V3u x0, V3u x1, V3u x2);
M2x3u m3x2u_transpose(M3x2u m);
M3x2u m3x2u_sum(M3x2u x, M3x2u y);
M3x2u m3x2u_sub(M3x2u x, M3x2u y);
M3x2u m3x2u_smulf(float s, M3x2u m);
M3x2u m3x2u_smuld(double s, M3x2u m);
M3x2u m3x2u_smuli(int s, M3x2u m);
M3x2u m3x2u_smulu(unsigned int s, M3x2u m);
M3x2u m3x2u_smuls(size_t s, M3x2u m);
V3u m3x2u_vmul(M3x2u m, V2u v);
M3x2u m3x2u_mul2x2(M3x2u m, M2u n);

M3x2s m3x2s(size_t x00, size_t x01, size_t x10, size_t x11, size_t x20, size_t x21);
M3x2s m3x2ss(size_t x);
M3x2s m3x2sc(V2s x0, V2s x1);
M3x2s m3x2sr(V3s x0, V3s x1, V3s x2);
M2x3s m3x2s_transpose(M3x2s m);
M3x2s m3x2s_sum(M3x2s x, M3x2s y);
M3x2s m3x2s_sub(M3x2s x, M3x2s y);
M3x2s m3x2s_smulf(float s, M3x2s m);
M3x2s m3x2s_smuld(double s, M3x2s m);
M3x2s m3x2s_smuli(int s, M3x2s m);
M3x2s m3x2s_smulu(unsigned int s, M3x2s m);
M3x2s m3x2s_smuls(size_t s, M3x2s m);
V3s m3x2s_vmul(M3x2s m, V2s v);
M3x2s m3x2s_mul2x2(M3x2s m, M2s n);

M3f m3f(float x00, float x01, float x02, float x10, float x11, float x12, float x20, float x21, float x22);
M3f m3fs(float x);
M3f m3fc(V3f x0, V3f x1, V3f x2);
M3f m3fr(V3f x0, V3f x1, V3f x2);
M3f m3f_transpose(M3f m);
M3f m3f_sum(M3f x, M3f y);
M3f m3f_sub(M3f x, M3f y);
M3f m3f_smulf(float s, M3f m);
M3f m3f_smuld(double s, M3f m);
M3f m3f_smuli(int s, M3f m);
M3f m3f_smulu(unsigned int s, M3f m);
M3f m3f_smuls(size_t s, M3f m);
V3f m3f_vmul(M3f m, V3f v);
M3x2f m3f_mul3x2(M3f m, M3x2f n);
M3f m3f_mul3x3(M3f m, M3f n);

M3d m3d(double x00, double x01, double x02, double x10, double x11, double x12, double x20, double x21, double x22);
M3d m3ds(double x);
M3d m3dc(V3d x0, V3d x1, V3d x2);
M3d m3dr(V3d x0, V3d x1, V3d x2);
M3d m3d_transpose(M3d m);
M3d m3d_sum(M3d x, M3d y);
M3d m3d_sub(M3d x, M3d y);
M3d m3d_smulf(float s, M3d m);
M3d m3d_smuld(double s, M3d m);
M3d m3d_smuli(int s, M3d m);
M3d m3d_smulu(unsigned int s, M3d m);
M3d m3d_smuls(size_t s, M3d m);
V3d m3d_vmul(M3d m, V3d v);
M3x2d m3d_mul3x2(M3d m, M3x2d n);
M3d m3d_mul3x3(M3d m, M3d n);

M3i m3i(int x00, int x01, int x02, int x10, int x11, int x12, int x20, int x21, int x22);
M3i m3is(int x);
M3i m3ic(V3i x0, V3i x1, V3i x2);
M3i m3ir(V3i x0, V3i x1, V3i x2);
M3i m3i_transpose(M3i m);
M3i m3i_sum(M3i x, M3i y);
M3i m3i_sub(M3i x, M3i y);
M3i m3i_smulf(float s, M3i m);
M3i m3i_smuld(double s, M3i m);
M3i m3i_smuli(int s, M3i m);
M3i m3i_smulu(unsigned int s, M3i m);
M3i m3i_smuls(size_t s, M3i m);
V3i m3i_vmul(M3i m, V3i v);
M3x2i m3i_mul3x2(M3i m, M3x2i n);
M3i m3i_mul3x3(M3i m, M3i n);

M3u m3u(unsigned int x00, unsigned int x01, unsigned int x02, unsigned int x10, unsigned int x11, unsigned int x12, unsigned int x20, unsigned int x21, unsigned int x22);
M3u m3us(unsigned int x);
M3u m3uc(V3u x0, V3u x1, V3u x2);
M3u m3ur(V3u x0, V3u x1, V3u x2);
M3u m3u_transpose(M3u m);
M3u m3u_sum(M3u x, M3u y);
M3u m3u_sub(M3u x, M3u y);
M3u m3u_smulf(float s, M3u m);
M3u m3u_smuld(double s, M3u m);
M3u m3u_smuli(int s, M3u m);
M3u m3u_smulu(unsigned int s, M3u m);
M3u m3u_smuls(size_t s, M3u m);
V3u m3u_vmul(M3u m, V3u v);
M3x2u m3u_mul3x2(M3u m, M3x2u n);
M3u m3u_mul3x3(M3u m, M3u n);

M3s m3s(size_t x00, size_t x01, size_t x02, size_t x10, size_t x11, size_t x12, size_t x20, size_t x21, size_t x22);
M3s m3ss(size_t x);
M3s m3sc(V3s x0, V3s x1, V3s x2);
M3s m3sr(V3s x0, V3s x1, V3s x2);
M3s m3s_transpose(M3s m);
M3s m3s_sum(M3s x, M3s y);
M3s m3s_sub(M3s x, M3s y);
M3s m3s_smulf(float s, M3s m);
M3s m3s_smuld(double s, M3s m);
M3s m3s_smuli(int s, M3s m);
M3s m3s_smulu(unsigned int s, M3s m);
M3s m3s_smuls(size_t s, M3s m);
V3s m3s_vmul(M3s m, V3s v);
M3x2s m3s_mul3x2(M3s m, M3x2s n);
M3s m3s_mul3x3(M3s m, M3s n);

M3x4f m3x4f(float x00, float x01, float x02, float x03, float x10, float x11, float x12, float x13, float x20, float x21, float x22, float x23);
M3x4f m3x4fs(float x);
M3x4f m3x4fc(V4f x0, V4f x1, V4f x2, V4f x3);
M3x4f m3x4fr(V3f x0, V3f x1, V3f x2);
M4x3f m3x4f_transpose(M3x4f m);
M3x4f m3x4f_sum(M3x4f x, M3x4f y);
M3x4f m3x4f_sub(M3x4f x, M3x4f y);
M3x4f m3x4f_smulf(float s, M3x4f m);
M3x4f m3x4f_smuld(double s, M3x4f m);
M3x4f m3x4f_smuli(int s, M3x4f m);
M3x4f m3x4f_smulu(unsigned int s, M3x4f m);
M3x4f m3x4f_smuls(size_t s, M3x4f m);
V3f m3x4f_vmul(M3x4f m, V4f v);
M3x2f m3x4f_mul4x2(M3x4f m, M4x2f n);
M3f m3x4f_mul4x3(M3x4f m, M4x3f n);
M3x4f m3x4f_mul4x4(M3x4f m, M4f n);

M3x4d m3x4d(double x00, double x01, double x02, double x03, double x10, double x11, double x12, double x13, double x20, double x21, double x22, double x23);
M3x4d m3x4ds(double x);
M3x4d m3x4dc(V4d x0, V4d x1, V4d x2, V4d x3);
M3x4d m3x4dr(V3d x0, V3d x1, V3d x2);
M4x3d m3x4d_transpose(M3x4d m);
M3x4d m3x4d_sum(M3x4d x, M3x4d y);
M3x4d m3x4d_sub(M3x4d x, M3x4d y);
M3x4d m3x4d_smulf(float s, M3x4d m);
M3x4d m3x4d_smuld(double s, M3x4d m);
M3x4d m3x4d_smuli(int s, M3x4d m);
M3x4d m3x4d_smulu(unsigned int s, M3x4d m);
M3x4d m3x4d_smuls(size_t s, M3x4d m);
V3d m3x4d_vmul(M3x4d m, V4d v);
M3x2d m3x4d_mul4x2(M3x4d m, M4x2d n);
M3d m3x4d_mul4x3(M3x4d m, M4x3d n);
M3x4d m3x4d_mul4x4(M3x4d m, M4d n);

M3x4i m3x4i(int x00, int x01, int x02, int x03, int x10, int x11, int x12, int x13, int x20, int x21, int x22, int x23);
M3x4i m3x4is(int x);
M3x4i m3x4ic(V4i x0, V4i x1, V4i x2, V4i x3);
M3x4i m3x4ir(V3i x0, V3i x1, V3i x2);
M4x3i m3x4i_transpose(M3x4i m);
M3x4i m3x4i_sum(M3x4i x, M3x4i y);
M3x4i m3x4i_sub(M3x4i x, M3x4i y);
M3x4i m3x4i_smulf(float s, M3x4i m);
M3x4i m3x4i_smuld(double s, M3x4i m);
M3x4i m3x4i_smuli(int s, M3x4i m);
M3x4i m3x4i_smulu(unsigned int s, M3x4i m);
M3x4i m3x4i_smuls(size_t s, M3x4i m);
V3i m3x4i_vmul(M3x4i m, V4i v);
M3x2i m3x4i_mul4x2(M3x4i m, M4x2i n);
M3i m3x4i_mul4x3(M3x4i m, M4x3i n);
M3x4i m3x4i_mul4x4(M3x4i m, M4i n);

M3x4u m3x4u(unsigned int x00, unsigned int x01, unsigned int x02, unsigned int x03, unsigned int x10, unsigned int x11, unsigned int x12, unsigned int x13, unsigned int x20, unsigned int x21, unsigned int x22, unsigned int x23);
M3x4u m3x4us(unsigned int x);
M3x4u m3x4uc(V4u x0, V4u x1, V4u x2, V4u x3);
M3x4u m3x4ur(V3u x0, V3u x1, V3u x2);
M4x3u m3x4u_transpose(M3x4u m);
M3x4u m3x4u_sum(M3x4u x, M3x4u y);
M3x4u m3x4u_sub(M3x4u x, M3x4u y);
M3x4u m3x4u_smulf(float s, M3x4u m);
M3x4u m3x4u_smuld(double s, M3x4u m);
M3x4u m3x4u_smuli(int s, M3x4u m);
M3x4u m3x4u_smulu(unsigned int s, M3x4u m);
M3x4u m3x4u_smuls(size_t s, M3x4u m);
V3u m3x4u_vmul(M3x4u m, V4u v);
M3x2u m3x4u_mul4x2(M3x4u m, M4x2u n);
M3u m3x4u_mul4x3(M3x4u m, M4x3u n);
M3x4u m3x4u_mul4x4(M3x4u m, M4u n);

M3x4s m3x4s(size_t x00, size_t x01, size_t x02, size_t x03, size_t x10, size_t x11, size_t x12, size_t x13, size_t x20, size_t x21, size_t x22, size_t x23);
M3x4s m3x4ss(size_t x);
M3x4s m3x4sc(V4s x0, V4s x1, V4s x2, V4s x3);
M3x4s m3x4sr(V3s x0, V3s x1, V3s x2);
M4x3s m3x4s_transpose(M3x4s m);
M3x4s m3x4s_sum(M3x4s x, M3x4s y);
M3x4s m3x4s_sub(M3x4s x, M3x4s y);
M3x4s m3x4s_smulf(float s, M3x4s m);
M3x4s m3x4s_smuld(double s, M3x4s m);
M3x4s m3x4s_smuli(int s, M3x4s m);
M3x4s m3x4s_smulu(unsigned int s, M3x4s m);
M3x4s m3x4s_smuls(size_t s, M3x4s m);
V3s m3x4s_vmul(M3x4s m, V4s v);
M3x2s m3x4s_mul4x2(M3x4s m, M4x2s n);
M3s m3x4s_mul4x3(M3x4s m, M4x3s n);
M3x4s m3x4s_mul4x4(M3x4s m, M4s n);

M4x2f m4x2f(float x00, float x01, float x10, float x11, float x20, float x21, float x30, float x31);
M4x2f m4x2fs(float x);
M4x2f m4x2fc(V2f x0, V2f x1);
M4x2f m4x2fr(V4f x0, V4f x1, V4f x2, V4f x3);
M2x4f m4x2f_transpose(M4x2f m);
M4x2f m4x2f_sum(M4x2f x, M4x2f y);
M4x2f m4x2f_sub(M4x2f x, M4x2f y);
M4x2f m4x2f_smulf(float s, M4x2f m);
M4x2f m4x2f_smuld(double s, M4x2f m);
M4x2f m4x2f_smuli(int s, M4x2f m);
M4x2f m4x2f_smulu(unsigned int s, M4x2f m);
M4x2f m4x2f_smuls(size_t s, M4x2f m);
V4f m4x2f_vmul(M4x2f m, V2f v);
M4x2f m4x2f_mul2x2(M4x2f m, M2f n);

M4x2d m4x2d(double x00, double x01, double x10, double x11, double x20, double x21, double x30, double x31);
M4x2d m4x2ds(double x);
M4x2d m4x2dc(V2d x0, V2d x1);
M4x2d m4x2dr(V4d x0, V4d x1, V4d x2, V4d x3);
M2x4d m4x2d_transpose(M4x2d m);
M4x2d m4x2d_sum(M4x2d x, M4x2d y);
M4x2d m4x2d_sub(M4x2d x, M4x2d y);
M4x2d m4x2d_smulf(float s, M4x2d m);
M4x2d m4x2d_smuld(double s, M4x2d m);
M4x2d m4x2d_smuli(int s, M4x2d m);
M4x2d m4x2d_smulu(unsigned int s, M4x2d m);
M4x2d m4x2d_smuls(size_t s, M4x2d m);
V4d m4x2d_vmul(M4x2d m, V2d v);
M4x2d m4x2d_mul2x2(M4x2d m, M2d n);

M4x2i m4x2i(int x00, int x01, int x10, int x11, int x20, int x21, int x30, int x31);
M4x2i m4x2is(int x);
M4x2i m4x2ic(V2i x0, V2i x1);
M4x2i m4x2ir(V4i x0, V4i x1, V4i x2, V4i x3);
M2x4i m4x2i_transpose(M4x2i m);
M4x2i m4x2i_sum(M4x2i x, M4x2i y);
M4x2i m4x2i_sub(M4x2i x, M4x2i y);
M4x2i m4x2i_smulf(float s, M4x2i m);
M4x2i m4x2i_smuld(double s, M4x2i m);
M4x2i m4x2i_smuli(int s, M4x2i m);
M4x2i m4x2i_smulu(unsigned int s, M4x2i m);
M4x2i m4x2i_smuls(size_t s, M4x2i m);
V4i m4x2i_vmul(M4x2i m, V2i v);
M4x2i m4x2i_mul2x2(M4x2i m, M2i n);

M4x2u m4x2u(unsigned int x00, unsigned int x01, unsigned int x10, unsigned int x11, unsigned int x20, unsigned int x21, unsigned int x30, unsigned int x31);
M4x2u m4x2us(unsigned int x);
M4x2u m4x2uc(V2u x0, V2u x1);
M4x2u m4x2ur(V4u x0, V4u x1, V4u x2, V4u x3);
M2x4u m4x2u_transpose(M4x2u m);
M4x2u m4x2u_sum(M4x2u x, M4x2u y);
M4x2u m4x2u_sub(M4x2u x, M4x2u y);
M4x2u m4x2u_smulf(float s, M4x2u m);
M4x2u m4x2u_smuld(double s, M4x2u m);
M4x2u m4x2u_smuli(int s, M4x2u m);
M4x2u m4x2u_smulu(unsigned int s, M4x2u m);
M4x2u m4x2u_smuls(size_t s, M4x2u m);
V4u m4x2u_vmul(M4x2u m, V2u v);
M4x2u m4x2u_mul2x2(M4x2u m, M2u n);

M4x2s m4x2s(size_t x00, size_t x01, size_t x10, size_t x11, size_t x20, size_t x21, size_t x30, size_t x31);
M4x2s m4x2ss(size_t x);
M4x2s m4x2sc(V2s x0, V2s x1);
M4x2s m4x2sr(V4s x0, V4s x1, V4s x2, V4s x3);
M2x4s m4x2s_transpose(M4x2s m);
M4x2s m4x2s_sum(M4x2s x, M4x2s y);
M4x2s m4x2s_sub(M4x2s x, M4x2s y);
M4x2s m4x2s_smulf(float s, M4x2s m);
M4x2s m4x2s_smuld(double s, M4x2s m);
M4x2s m4x2s_smuli(int s, M4x2s m);
M4x2s m4x2s_smulu(unsigned int s, M4x2s m);
M4x2s m4x2s_smuls(size_t s, M4x2s m);
V4s m4x2s_vmul(M4x2s m, V2s v);
M4x2s m4x2s_mul2x2(M4x2s m, M2s n);

M4x3f m4x3f(float x00, float x01, float x02, float x10, float x11, float x12, float x20, float x21, float x22, float x30, float x31, float x32);
M4x3f m4x3fs(float x);
M4x3f m4x3fc(V3f x0, V3f x1, V3f x2);
M4x3f m4x3fr(V4f x0, V4f x1, V4f x2, V4f x3);
M3x4f m4x3f_transpose(M4x3f m);
M4x3f m4x3f_sum(M4x3f x, M4x3f y);
M4x3f m4x3f_sub(M4x3f x, M4x3f y);
M4x3f m4x3f_smulf(float s, M4x3f m);
M4x3f m4x3f_smuld(double s, M4x3f m);
M4x3f m4x3f_smuli(int s, M4x3f m);
M4x3f m4x3f_smulu(unsigned int s, M4x3f m);
M4x3f m4x3f_smuls(size_t s, M4x3f m);
V4f m4x3f_vmul(M4x3f m, V3f v);
M4x2f m4x3f_mul3x2(M4x3f m, M3x2f n);
M4x3f m4x3f_mul3x3(M4x3f m, M3f n);

M4x3d m4x3d(double x00, double x01, double x02, double x10, double x11, double x12, double x20, double x21, double x22, double x30, double x31, double x32);
M4x3d m4x3ds(double x);
M4x3d m4x3dc(V3d x0, V3d x1, V3d x2);
M4x3d m4x3dr(V4d x0, V4d x1, V4d x2, V4d x3);
M3x4d m4x3d_transpose(M4x3d m);
M4x3d m4x3d_sum(M4x3d x, M4x3d y);
M4x3d m4x3d_sub(M4x3d x, M4x3d y);
M4x3d m4x3d_smulf(float s, M4x3d m);
M4x3d m4x3d_smuld(double s, M4x3d m);
M4x3d m4x3d_smuli(int s, M4x3d m);
M4x3d m4x3d_smulu(unsigned int s, M4x3d m);
M4x3d m4x3d_smuls(size_t s, M4x3d m);
V4d m4x3d_vmul(M4x3d m, V3d v);
M4x2d m4x3d_mul3x2(M4x3d m, M3x2d n);
M4x3d m4x3d_mul3x3(M4x3d m, M3d n);

M4x3i m4x3i(int x00, int x01, int x02, int x10, int x11, int x12, int x20, int x21, int x22, int x30, int x31, int x32);
M4x3i m4x3is(int x);
M4x3i m4x3ic(V3i x0, V3i x1, V3i x2);
M4x3i m4x3ir(V4i x0, V4i x1, V4i x2, V4i x3);
M3x4i m4x3i_transpose(M4x3i m);
M4x3i m4x3i_sum(M4x3i x, M4x3i y);
M4x3i m4x3i_sub(M4x3i x, M4x3i y);
M4x3i m4x3i_smulf(float s, M4x3i m);
M4x3i m4x3i_smuld(double s, M4x3i m);
M4x3i m4x3i_smuli(int s, M4x3i m);
M4x3i m4x3i_smulu(unsigned int s, M4x3i m);
M4x3i m4x3i_smuls(size_t s, M4x3i m);
V4i m4x3i_vmul(M4x3i m, V3i v);
M4x2i m4x3i_mul3x2(M4x3i m, M3x2i n);
M4x3i m4x3i_mul3x3(M4x3i m, M3i n);

M4x3u m4x3u(unsigned int x00, unsigned int x01, unsigned int x02, unsigned int x10, unsigned int x11, unsigned int x12, unsigned int x20, unsigned int x21, unsigned int x22, unsigned int x30, unsigned int x31, unsigned int x32);
M4x3u m4x3us(unsigned int x);
M4x3u m4x3uc(V3u x0, V3u x1, V3u x2);
M4x3u m4x3ur(V4u x0, V4u x1, V4u x2, V4u x3);
M3x4u m4x3u_transpose(M4x3u m);
M4x3u m4x3u_sum(M4x3u x, M4x3u y);
M4x3u m4x3u_sub(M4x3u x, M4x3u y);
M4x3u m4x3u_smulf(float s, M4x3u m);
M4x3u m4x3u_smuld(double s, M4x3u m);
M4x3u m4x3u_smuli(int s, M4x3u m);
M4x3u m4x3u_smulu(unsigned int s, M4x3u m);
M4x3u m4x3u_smuls(size_t s, M4x3u m);
V4u m4x3u_vmul(M4x3u m, V3u v);
M4x2u m4x3u_mul3x2(M4x3u m, M3x2u n);
M4x3u m4x3u_mul3x3(M4x3u m, M3u n);

M4x3s m4x3s(size_t x00, size_t x01, size_t x02, size_t x10, size_t x11, size_t x12, size_t x20, size_t x21, size_t x22, size_t x30, size_t x31, size_t x32);
M4x3s m4x3ss(size_t x);
M4x3s m4x3sc(V3s x0, V3s x1, V3s x2);
M4x3s m4x3sr(V4s x0, V4s x1, V4s x2, V4s x3);
M3x4s m4x3s_transpose(M4x3s m);
M4x3s m4x3s_sum(M4x3s x, M4x3s y);
M4x3s m4x3s_sub(M4x3s x, M4x3s y);
M4x3s m4x3s_smulf(float s, M4x3s m);
M4x3s m4x3s_smuld(double s, M4x3s m);
M4x3s m4x3s_smuli(int s, M4x3s m);
M4x3s m4x3s_smulu(unsigned int s, M4x3s m);
M4x3s m4x3s_smuls(size_t s, M4x3s m);
V4s m4x3s_vmul(M4x3s m, V3s v);
M4x2s m4x3s_mul3x2(M4x3s m, M3x2s n);
M4x3s m4x3s_mul3x3(M4x3s m, M3s n);

M4f m4f(float x00, float x01, float x02, float x03, float x10, float x11, float x12, float x13, float x20, float x21, float x22, float x23, float x30, float x31, float x32, float x33);
M4f m4fs(float x);
M4f m4fc(V4f x0, V4f x1, V4f x2, V4f x3);
M4f m4fr(V4f x0, V4f x1, V4f x2, V4f x3);
M4f m4f_transpose(M4f m);
M4f m4f_sum(M4f x, M4f y);
M4f m4f_sub(M4f x, M4f y);
M4f m4f_smulf(float s, M4f m);
M4f m4f_smuld(double s, M4f m);
M4f m4f_smuli(int s, M4f m);
M4f m4f_smulu(unsigned int s, M4f m);
M4f m4f_smuls(size_t s, M4f m);
V4f m4f_vmul(M4f m, V4f v);
M4x2f m4f_mul4x2(M4f m, M4x2f n);
M4x3f m4f_mul4x3(M4f m, M4x3f n);
M4f m4f_mul4x4(M4f m, M4f n);

M4d m4d(double x00, double x01, double x02, double x03, double x10, double x11, double x12, double x13, double x20, double x21, double x22, double x23, double x30, double x31, double x32, double x33);
M4d m4ds(double x);
M4d m4dc(V4d x0, V4d x1, V4d x2, V4d x3);
M4d m4dr(V4d x0, V4d x1, V4d x2, V4d x3);
M4d m4d_transpose(M4d m);
M4d m4d_sum(M4d x, M4d y);
M4d m4d_sub(M4d x, M4d y);
M4d m4d_smulf(float s, M4d m);
M4d m4d_smuld(double s, M4d m);
M4d m4d_smuli(int s, M4d m);
M4d m4d_smulu(unsigned int s, M4d m);
M4d m4d_smuls(size_t s, M4d m);
V4d m4d_vmul(M4d m, V4d v);
M4x2d m4d_mul4x2(M4d m, M4x2d n);
M4x3d m4d_mul4x3(M4d m, M4x3d n);
M4d m4d_mul4x4(M4d m, M4d n);

M4i m4i(int x00, int x01, int x02, int x03, int x10, int x11, int x12, int x13, int x20, int x21, int x22, int x23, int x30, int x31, int x32, int x33);
M4i m4is(int x);
M4i m4ic(V4i x0, V4i x1, V4i x2, V4i x3);
M4i m4ir(V4i x0, V4i x1, V4i x2, V4i x3);
M4i m4i_transpose(M4i m);
M4i m4i_sum(M4i x, M4i y);
M4i m4i_sub(M4i x, M4i y);
M4i m4i_smulf(float s, M4i m);
M4i m4i_smuld(double s, M4i m);
M4i m4i_smuli(int s, M4i m);
M4i m4i_smulu(unsigned int s, M4i m);
M4i m4i_smuls(size_t s, M4i m);
V4i m4i_vmul(M4i m, V4i v);
M4x2i m4i_mul4x2(M4i m, M4x2i n);
M4x3i m4i_mul4x3(M4i m, M4x3i n);
M4i m4i_mul4x4(M4i m, M4i n);

M4u m4u(unsigned int x00, unsigned int x01, unsigned int x02, unsigned int x03, unsigned int x10, unsigned int x11, unsigned int x12, unsigned int x13, unsigned int x20, unsigned int x21, unsigned int x22, unsigned int x23, unsigned int x30, unsigned int x31, unsigned int x32, unsigned int x33);
M4u m4us(unsigned int x);
M4u m4uc(V4u x0, V4u x1, V4u x2, V4u x3);
M4u m4ur(V4u x0, V4u x1, V4u x2, V4u x3);
M4u m4u_transpose(M4u m);
M4u m4u_sum(M4u x, M4u y);
M4u m4u_sub(M4u x, M4u y);
M4u m4u_smulf(float s, M4u m);
M4u m4u_smuld(double s, M4u m);
M4u m4u_smuli(int s, M4u m);
M4u m4u_smulu(unsigned int s, M4u m);
M4u m4u_smuls(size_t s, M4u m);
V4u m4u_vmul(M4u m, V4u v);
M4x2u m4u_mul4x2(M4u m, M4x2u n);
M4x3u m4u_mul4x3(M4u m, M4x3u n);
M4u m4u_mul4x4(M4u m, M4u n);

M4s m4s(size_t x00, size_t x01, size_t x02, size_t x03, size_t x10, size_t x11, size_t x12, size_t x13, size_t x20, size_t x21, size_t x22, size_t x23, size_t x30, size_t x31, size_t x32, size_t x33);
M4s m4ss(size_t x);
M4s m4sc(V4s x0, V4s x1, V4s x2, V4s x3);
M4s m4sr(V4s x0, V4s x1, V4s x2, V4s x3);
M4s m4s_transpose(M4s m);
M4s m4s_sum(M4s x, M4s y);
M4s m4s_sub(M4s x, M4s y);
M4s m4s_smulf(float s, M4s m);
M4s m4s_smuld(double s, M4s m);
M4s m4s_smuli(int s, M4s m);
M4s m4s_smulu(unsigned int s, M4s m);
M4s m4s_smuls(size_t s, M4s m);
V4s m4s_vmul(M4s m, V4s v);
M4x2s m4s_mul4x2(M4s m, M4x2s n);
M4x3s m4s_mul4x3(M4s m, M4x3s n);
M4s m4s_mul4x4(M4s m, M4s n);

M4f ortho(float left, float right, float top, float bottom, float near, float far);
M4f perspective(const float fov, const float aspect, const float near, const float far);
M4f rotatev(float angle, V3f axis);
M4f rotates(float angle, float x, float y, float z);

#endif // CLAL_H_

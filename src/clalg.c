#include "clalg_matrix.h"

int main(void)
{
    FILE *HEADER = fopen("clal.h", "w+");

    fprintf(HEADER, "#ifndef CLAL_H_\n");
    fprintf(HEADER, "#define CLAL_H_\n");
    fprintf(HEADER, "\n#include <stdlib.h>\n#include <math.h>\n\n");
    fprintf(HEADER, "#ifndef M_PI\n#define M_PI 3.14159265358979323846\n#endif\n");
//TYPES DEFINITION

    for(size_t n = 2; n <= SUPPORTED_DIMS; ++n)
    {
	for(Type_Def_Type t = 0; t < TYPE_VEC_COUNT; ++t)
	{
	    gen_vector_def(HEADER, n, types[t]);
	}
    }
    fprintf(HEADER, "\n");
    for(size_t n = 2; n <= SUPPORTED_DIMS; ++n)
    {
	for(size_t m = 2; m <= SUPPORTED_DIMS; ++m)
	{
	    for(Type_Def_Type t = 0; t < TYPE_VEC_COUNT; ++t)
		gen_matrix_def(HEADER, n, m, types[t]);
	}
    }
    fprintf(HEADER, "\n");

//VECTOR HEADER

    for(size_t n = 2; n <= SUPPORTED_DIMS; ++n)
    {
	for(Type_Def_Type t = 0; t < TYPE_VEC_COUNT; ++t)
	{
	    gen_vector_print_fmt(HEADER, n, types[t]);

	    gen_vector_ctor_decl(HEADER, n, types[t]);
	    gen_vector_sctor_decl(HEADER, n, types[t]);

	    for(Op_Type o = 0; o < OP_COUNT; ++o)
	    {
		gen_vector_op_decl(HEADER, n, types[t], operators[o]);
	    }
	    for(Type_Def_Type j = 0; j < TYPE_VEC_COUNT; ++j)
		gen_vector_smul_decl(HEADER, n, types[t], types[j]);

	    gen_scalar_prod_decl(HEADER, n, types[t]);
	    gen_vector_norm_decl(HEADER, n, types[t]);

	    fprintf(HEADER, "\n");
	}
	fprintf(HEADER, "\n");
    }
    fprintf(stdout, "written vector header\n");

//MATRICES HEADER

    for(size_t n = 2; n <= SUPPORTED_DIMS; ++n)
    {
	for(size_t m = 2; m <= SUPPORTED_DIMS; ++m)
	{
	    for(Type_Def_Type t = 0; t < TYPE_VEC_COUNT; ++t)
	    {
		gen_matrix_ctor_decl(HEADER, n, m, types[t]);
		gen_matrix_sctor_decl(HEADER, n, m, types[t]);
		gen_matrix_cctor_decl(HEADER, n, m, types[t]);
		gen_matrix_rctor_decl(HEADER, n, m, types[t]);
		gen_matrix_transposition_decl(HEADER, n, m, types[t]);
		for(Op_Type o = 0; o < OP_COUNT; ++o)
		{
		    gen_matrix_op_decl(HEADER, n, m, types[t], operators[o]);
		}

		for(Type_Def_Type j = 0; j < TYPE_VEC_COUNT; ++j)
		    gen_matrix_smul_decl(HEADER, n, m, types[t], types[j]);

		gen_matrix_vec_mul_decl(HEADER, n, m, types[t]);
		gen_matrix_mul_decl(HEADER, n, m, types[t]);
		fprintf(HEADER, "\n");
	    }
	}
    }
    gen_transformation_matrix_decl(HEADER);

    fprintf(HEADER, "\n");
    fprintf(HEADER, "#endif // CLAL_H_\n");

    fclose(HEADER);

    FILE *OBJECT = fopen("clal.c", "w+");

    fprintf(stdout, "written header\nstarting writing object\n");
    fprintf(OBJECT, "#include \"clal.h\"\n\n");

//VECTOR IMPL
    for(size_t n = 2; n <= SUPPORTED_DIMS; ++n)
    {
	for(Type_Def_Type t = 0; t < TYPE_VEC_COUNT; ++t)
	{
	    gen_vector_ctor_impl(OBJECT, n, types[t]);
	    fprintf(OBJECT, "\n");

	    gen_vector_sctor_impl(OBJECT, n, types[t]);
	    fprintf(OBJECT, "\n");

	    for(Op_Type o = 0; o < OP_COUNT; ++o)
	    {
		gen_vector_op_impl(OBJECT, n, types[t], operators[o]);
		fprintf(OBJECT, "\n");
	    }

	    for(Type_Def_Type j = 0; j < TYPE_VEC_COUNT; ++j)
		gen_vector_smul_impl(OBJECT, n, types[t], types[j]);

	    fprintf(OBJECT, "\n");
	    gen_scalar_prod_impl(OBJECT, n, types[t]);
	    gen_vector_norm_impl(OBJECT, n, types[t]);
	    fprintf(OBJECT, "\n");
	}
    }

//MATRICES IMPL

    for(size_t n = 2; n <= SUPPORTED_DIMS; ++n)
    {
	for(size_t m = 2; m <= SUPPORTED_DIMS; ++m)
	{
	    for(Type_Def_Type t = 0; t < TYPE_VEC_COUNT; ++t)
	    {
		gen_matrix_ctor_impl(OBJECT, n, m, types[t]);
		gen_matrix_sctor_impl(OBJECT, n, m, types[t]);
		gen_matrix_cctor_impl(OBJECT, n, m, types[t]);
		gen_matrix_rctor_impl(OBJECT, n, m, types[t]);
		gen_matrix_transposition_impl(HEADER, n, m, types[t]);
		for(Op_Type o = 0; o < OP_COUNT; ++o)
		{
		    gen_matrix_op_impl(OBJECT, n, m, types[t], operators[o]);
		}
		for(Type_Def_Type j = 0; j < TYPE_VEC_COUNT; ++j)
		    gen_matrix_smul_impl(OBJECT, n, m, types[t], types[j]);

		gen_matrix_vec_mul_impl(HEADER, n, m, types[t]);
		gen_matrix_mul_impl(HEADER, n, m, types[t]);
		fprintf(OBJECT, "\n");
	    }
	}
    }
    gen_transformation_matrix_impl(HEADER);

    //fprintf(OBJECT, "\n");
    // fprintf(stdout, "#endif //CLAL_IMPL\n");
    fclose(OBJECT);
    return 0;
}

#ifndef COMMON_CLALG_H
#define COMMON_CLALG_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>

#define SUPPORTED_DIMS 4

#define HEADER hdr
#define OBJECT obj

/*
 *  todo:
 *  matrix constructor: cctor rctor ctor
 *  matrix vector multiplication
 *  matrix matrix multiplication
 *
 *
 */

typedef struct
{
    const char *op;
    const char *suffix;
} Op;

typedef enum
{
    OP_ADD = 0,
    OP_SUB,
//    OP_MUL,
//    OP_DIV,
    OP_COUNT
} Op_Type;

Op operators[OP_COUNT] =
{
    [OP_ADD] = {.op = "+=", .suffix = "sum"},
    [OP_SUB] = {.op = "-=", .suffix = "sub"},
//    [OP_MUL] = {.op = "*=", .suffix = "mul"},
//    [OP_DIV] = {.op = "/=", .suffix = "div"}
};

typedef struct
{
    const char *name;
    const char *suffix;
} Type_Def;

typedef enum
{
    TYPE_VEC_FLOAT = 0,
    TYPE_VEC_DOUBLE,
    TYPE_VEC_INT,
    TYPE_VEC_UNSIGNED_INT,
    TYPE_VEC_SIZE_T,
    TYPE_VEC_COUNT
} Type_Def_Type;

Type_Def types[TYPE_VEC_COUNT] =
{
   [TYPE_VEC_FLOAT]        = {.name = "float", .suffix = "f"},
   [TYPE_VEC_DOUBLE]       = {.name = "double", .suffix = "d"},
   [TYPE_VEC_INT]          = {.name = "int", .suffix = "i"},
   [TYPE_VEC_UNSIGNED_INT] = {.name = "unsigned int", .suffix = "u"},
   [TYPE_VEC_SIZE_T]       = {.name = "size_t", .suffix = "s"}
};

typedef struct { char data[128]; } Short_String;

#if defined(__GNUC__) || defined(__clang__)
#define CHECK_SHORTF_FMT(x,y) __attribute__ ((format(printf, x, y)))
#else
#define CHECK_SHORTF_FMT(...)
#endif

CHECK_SHORTF_FMT(1, 2) Short_String shortf(const char *fmt, ...)
{
    Short_String result = {0};

    va_list args;
    va_start(args, fmt);

    int n = vsnprintf(result.data, sizeof(result.data), fmt, args);

    assert(n >= 0);
    assert( (size_t) n + 1 <= sizeof(result.data));

    va_end(args);
    return result;
}

#endif

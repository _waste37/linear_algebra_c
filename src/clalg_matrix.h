#ifndef CLALG_MATRIX_H
#define CLALG_MATRIX_H

#include "clalg_vector.h"
//#include "clalg_matrix.h"
//MATRICES DEFINITIONS

Short_String type_mat_name(size_t n, size_t m, Type_Def type)
{
    if(m != n)
	return shortf("M%zux%zu%s", n, m, type.suffix);
    else
	return shortf("M%zu%s", n, type.suffix);
}

Short_String type_mat_prefix(size_t n, size_t m, Type_Def type)
{
    if(m != n)
	return shortf("m%zux%zu%s", n, m, type.suffix);
    else
	return shortf("m%zu%s", n, type.suffix);
}

void gen_matrix_def(FILE *stream, size_t n, size_t m, Type_Def type)
{
    if( m != n)
	fprintf(stream, "typedef struct { %s c[%zu][%zu]; } M%zux%zu%s;\n",
		type.name, n, m, n, m, type.suffix);
    else
	fprintf(stream, "typedef struct { %s c[%zu][%zu]; } M%zu%s;\n",
		 type.name, n, n, n, type.suffix);
}

void gen_matrix_ctor_decl(FILE *stream, size_t n, size_t m, Type_Def type)
{
     fprintf(stream, "%s %s(", type_mat_name(n, m, type).data,
	    type_mat_prefix(n, m, type).data);
     for(size_t i = 0 ; i < n; ++i)
     {
	 for(size_t j = 0; j < m; ++j)
	 {
	     if(i > 0 || j > 0) fprintf(stream, ", ");
	     fprintf(stream, "%s x%zu%zu", type.name, i, j);
	 }
     }
     fprintf(stream, ");\n");
}

void gen_matrix_cctor_decl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    Short_String col_vec = type_vec_name(m, type);
    fprintf(stream, "%s %sc(", type_mat_name(n, m, type).data, type_mat_prefix(n, m, type).data);
    for(size_t i = 0; i < m; ++i)
    {
	if(i > 0) fprintf(stream, ", ");
	fprintf(stream, "%s x%zu", col_vec.data, i);
    }
    fprintf(stream, ");\n");
}

void gen_matrix_rctor_decl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    Short_String col_vec = type_vec_name(n, type);
    fprintf(stream, "%s %sr(", type_mat_name(n, m, type).data, type_mat_prefix(n, m, type).data);
    for(size_t i = 0; i < n; ++i)
    {
	if(i > 0) fprintf(stream, ", ");
	fprintf(stream, "%s x%zu", col_vec.data, i);
    }
    fprintf(stream, ");\n");
}

void gen_matrix_sctor_decl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    fprintf(stream, "%s %ss(%s x);\n", type_mat_name(n, m, type).data,
	    type_mat_prefix(n, m, type).data, type.name);
}

void gen_matrix_op_sig(FILE *stream, size_t n, size_t m, Type_Def type, Op op)
{
    Short_String type_str = type_mat_name(n, m, type);
    Short_String f_prefix = type_mat_prefix(n, m, type);
    fprintf(stream, "%s %s_%s(%s x, %s y)", type_str.data,
	    f_prefix.data, op.suffix,
	    type_str.data, type_str.data);
}

void gen_matrix_op_decl(FILE *stream, size_t n, size_t m, Type_Def type, Op op)
{
    gen_matrix_op_sig(stream, n, m, type, op);
    fprintf(stream, ";\n");
}

void gen_matrix_smul_decl(FILE *stream, size_t n, size_t m, Type_Def type, Type_Def s_type)
{
    Short_String type_str = type_mat_name(n, m, type);
    Short_String f_prefix = type_mat_prefix(n, m, type);

    fprintf(stream, "%s %s_smul%s(%s s, %s m);\n",
	    type_str.data, f_prefix.data,
	    s_type.suffix, s_type.name, type_str.data);
}

void gen_matrix_vec_mul_decl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    fprintf(stream, "%s %s_vmul(%s m, %s v);\n",
	    type_vec_name(n, type).data,
	    type_mat_prefix(n, m, type).data,
	    type_mat_name(n, m, type).data,
	    type_vec_name(m, type).data);
}

void gen_matrix_mul_decl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    for(size_t j = 2; j <= m; ++j)
    {
	fprintf(stream, "%s %s_mul%zux%zu(%s m, %s n);\n",
		type_mat_name(n, j, type).data,
		type_mat_prefix(n, m, type).data,
		m, j,
		type_mat_name(n, m, type).data,
		type_mat_name(m, j, type).data);
    }
}

void gen_matrix_transposition_decl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    fprintf(stream, "%s %s_transpose(%s m);\n",
	    type_mat_name(m, n, type).data,
	    type_mat_prefix(n, m, type).data,
	    type_mat_name(n, m, type).data);
}

void gen_ortho_matrix_decl(FILE *stream)
{
    fprintf(stream, "M4f ortho(float left, float right, float top, float bottom, float near, float far);\n");
}

void gen_perspective_matrix_decl(FILE *stream)
{
    fprintf(stream, "M4f perspective(const float fov, const float aspect, const float near, const float far);\n");
}

void gen_scaling_matrix_decl()
{

}

void gen_rotation_matrix_decl(FILE *stream)
{
    fprintf(stream, "M4f rotatev(float angle, V3f axis);\n");
    fprintf(stream, "M4f rotates(float angle, float x, float y, float z);\n");
}

void gen_translation_matrix_decl()
{

}

void gen_transformation_matrix_decl(FILE *stream)
{
    gen_ortho_matrix_decl(stream);
    gen_perspective_matrix_decl(stream);
    gen_rotation_matrix_decl(stream);
}

void gen_matrix_ctor_impl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    fprintf(stream, "%s %s(", type_mat_name(n, m, type).data,
	    type_mat_prefix(n, m, type).data);
     for(size_t i = 0 ; i < n; ++i)
     {
	 for(size_t j = 0; j < m; ++j)
	 {
	     if(i >0 || j > 0) fprintf(stream, ", ");
	     fprintf(stream, "%s x%zu%zu", type.name, i, j);
	 }
     }

     fprintf(stream, ")\n{\n    %s m;\n",  type_mat_name(n, m, type).data);

     for(size_t i = 0 ; i < n; ++i)
     {
	 fprintf(stream, "    ");
	 for(size_t j = 0; j < m; ++j)
	 {
	     fprintf(stream, "m.c[%zu][%zu] = x%zu%zu; ", i, j, i, j);
	 }
	 fprintf(stream, "\n");
     }
     fprintf(stream, "    return m;\n}\n");
}

void gen_matrix_cctor_impl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    Short_String type_str = type_mat_name(n, m, type);
    Short_String f_prefix = type_mat_prefix(n, m, type);

    Short_String col_vec = type_vec_name(m, type);
    fprintf(stream, "%s %sc(", type_str.data, f_prefix.data);
    for(size_t i = 0; i < m; ++i)
    {
	if(i > 0) fprintf(stream, ", ");
	fprintf(stream, "%s x%zu", col_vec.data, i);
    }
    fprintf(stream, ")\n{\n");
    fprintf(stream, "    %s m;\n", type_str.data);
    for(size_t i = 0; i < m; ++i)
    {
	fprintf(stream, "    for(int i = 0; i < %zu; ++i)", n);
	fprintf(stream, " m.c[i][%zu] = x%zu.c[i];\n", i, i);
    }
    fprintf(stream, "    return m;\n}\n");
}

void gen_matrix_rctor_impl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    Short_String type_str = type_mat_name(n, m, type);
    Short_String f_prefix = type_mat_prefix(n, m, type);

    Short_String col_vec = type_vec_name(n, type);
    fprintf(stream, "%s %sr(", type_str.data, f_prefix.data);
    for(size_t i = 0; i < n; ++i)
    {
	if(i > 0) fprintf(stream, ", ");
	fprintf(stream, "%s x%zu", col_vec.data, i);
    }
    fprintf(stream, ")\n{\n");
    fprintf(stream, "    %s m;\n", type_str.data);
    for(size_t i = 0; i < n; ++i)
    {
	fprintf(stream, "    for(int i = 0; i < %zu; ++i)", m);
	fprintf(stream, " m.c[%zu][i] = x%zu.c[i];\n", i, i);
    }
    fprintf(stream, "    return m;\n}\n");
}

void gen_matrix_sctor_impl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    Short_String type_str = type_mat_name(n, m, type);
    Short_String f_prefix = type_mat_prefix(n, m, type);

    fprintf(stream, "%s %ss(%s x)\n{\n", type_str.data,
	    f_prefix.data, type.name);
    fprintf(stream, "    %s m;\n", type_str.data);
    fprintf(stream, "    for(int i = 0; i < %zu; ++i)\n", n);
    fprintf(stream, "        for(int j = 0; j < %zu; ++j)\n        {\n", m);
    fprintf(stream, "             if(i == j) m.c[i][j] = x;\n");
    fprintf(stream, "             else m.c[i][j] = 0;\n        }\n");
    fprintf(stream, "    return m;\n}\n");
}


void gen_matrix_op_impl(FILE *stream, size_t n, size_t m, Type_Def type, Op op)
{
    gen_matrix_op_sig(stream, n, m, type, op);
    fprintf(stream, "\n{\n");
    fprintf(stream, "    for(int i = 0; i < %zu; ++i)\n", n);
    fprintf(stream, "        for(int j = 0; j < %zu; ++j)", m);
    fprintf(stream, " x.c[i][j] += y.c[i][j];\n");
    fprintf(stream, "    return x;\n}\n");
}

void gen_matrix_smul_impl(FILE *stream, size_t n, size_t m, Type_Def type, Type_Def s_type)
{
    Short_String type_str = type_mat_name(n, m, type);
    Short_String f_prefix = type_mat_prefix(n, m, type);

    fprintf(stream, "%s %s_smul%s(%s s, %s m)",
	    type_str.data, f_prefix.data,
	    s_type.suffix, s_type.name, type_str.data);
    fprintf(stream, "\n{\n");
    fprintf(stream, "    for(int i = 0; i < %zu; ++i)\n", n);
    fprintf(stream, "        for(int j = 0; j < %zu; ++j)", m);
    fprintf(stream, " m.c[i][j] *= s;\n");
    fprintf(stream, "    return m;\n}\n");
}

void gen_matrix_vec_mul_impl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    Short_String vec_type = type_vec_name(n, type);
    fprintf(stream, "%s %s_vmul(%s m, %s v)\n{\n    %s r;\n",
	    vec_type.data,
	    type_mat_prefix(n, m, type).data,
	    type_mat_name(n, m, type).data,
	    type_vec_name(m, type).data, vec_type.data);
    fprintf(stream, "    for(int i = 0; i < %zu; ++i)\n    {\n", n);
    fprintf(stream, "        %s acc = 0;\n", type.name);
    fprintf(stream, "        for(int j = 0; j < %zu; ++j) acc += m.c[i][j] * v.c[j];\n", m);
    fprintf(stream, "        r.c[i] = acc;\n");
    fprintf(stream, "    }\n    return r;\n}\n");
}

void gen_matrix_mul_impl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    for(size_t j = 2; j <= m; ++j)
    {
	fprintf(stream, "%s %s_mul%zux%zu(%s m, %s n)\n{\n",
		type_mat_name(n, j, type).data,
		type_mat_prefix(n, m, type).data,
		m, j,
		type_mat_name(n, m, type).data,
		type_mat_name(m, j, type).data);
	fprintf(stream, "    %s r;\n", type_mat_name(n, j, type).data);
	fprintf(stream, "    for(int i = 0; i < %zu; ++i)\n    {\n", n);
	fprintf(stream, "        for(int j = 0; j < %zu; ++j)\n        {\n", j);
	fprintf(stream, "            %s acc = 0;\n", type.name);
	fprintf(stream, "            for(int k = 0; k < %zu; ++k)", m);
	fprintf(stream, " acc += m.c[i][k] * n.c[k][j];\n");
	fprintf(stream, "            r.c[i][j] = acc;        }\n");
	fprintf(stream, "    }\n return r;\n}\n");
    }
}

void gen_ortho_matrix_impl(FILE *stream)
{
    fprintf(stream, "M4f ortho(float left, float right, float top, float bottom, float near, float far)\n{\n");
    fprintf(stream, "    M4f m = m4fs(0.0f);\n");
    fprintf(stream, "    m.c[0][0] = 2 / (right - left);\n");
    fprintf(stream, "    m.c[1][1] = 2 / (top - bottom);\n");
    fprintf(stream, "    m.c[2][2] = -2 / (far - near);\n");
    fprintf(stream, "    m.c[3][0] = -(right + left) / (right - left);\n");
    fprintf(stream, "    m.c[3][1] = -(top + bottom) / (top - bottom);\n");
    fprintf(stream, "    m.c[3][2] = -(far + near) / (far - near);\n");
    fprintf(stream, "    m.c[3][3] = 1;\n    return m;\n}\n");
}

void gen_perspective_matrix_impl(FILE *stream)
{
    fprintf(stream, "M4f perspective(float fov, float aspect, float near, float far)\n{\n");
    fprintf(stream, "    float scale = tan(fov * 0.5 * M_PI / 180) * near;\n");
    fprintf(stream, "    float r = aspect*scale, l = -r, t = scale, b = -t;\n");
    fprintf(stream, "    M4f m = m4fs(0.0f);\n");
    fprintf(stream, "    m.c[0][0] = 2 * near / (r - l);\n");
    fprintf(stream, "    m.c[1][1] = 2 * near / (t - b);\n");
    fprintf(stream, "    m.c[2][0] = (r + l) / (r - l);\n");
    fprintf(stream, "    m.c[2][1] = (t + b) / (t - b);\n");
    fprintf(stream, "    m.c[2][2] = -(far + near) / (far - near);\n");
    fprintf(stream, "    m.c[2][3] = -1;\n");
    fprintf(stream, "    m.c[3][2] = -2 * far * near / (far - near);\n");
    fprintf(stream, "    return m;\n");
    fprintf(stream, "}\n");
}

void gen_scaling_matrix_impl()
{
}

void gen_rotation_matrix_impl(FILE *stream)
{
    fprintf(stream, "M4f rotatev(float angle, V3f axis)\n{\n");
    fprintf(stream, "    M4f m = m4fs(0.0f);\n");
    fprintf(stream, "    m.c[0][0] = axis.c[0]*axis.c[0]*(1-cos(angle)) + cos(angle);\n");
    fprintf(stream, "    m.c[0][1] = axis.c[0]*axis.c[1]*(1-cos(angle)) - axis.c[2]*sin(angle);\n");
    fprintf(stream, "    m.c[0][2] = axis.c[0]*axis.c[2]*(1-cos(angle)) + axis.c[1]*sin(angle);\n");
    fprintf(stream, "    m.c[1][0] = axis.c[0]*axis.c[1]*(1-cos(angle)) + axis.c[2]*sin(angle);\n");
    fprintf(stream, "    m.c[1][1] = axis.c[1]*axis.c[1]*(1-cos(angle)) + cos(angle);\n");
    fprintf(stream, "    m.c[1][2] = axis.c[2]*axis.c[1]*(1-cos(angle)) - axis.c[0]*sin(angle);\n");
    fprintf(stream, "    m.c[2][0] = axis.c[0]*axis.c[2]*(1-cos(angle)) - axis.c[1]*sin(angle);\n");
    fprintf(stream, "    m.c[2][1] = axis.c[2]*axis.c[1]*(1-cos(angle)) + axis.c[0]*sin(angle);\n");
    fprintf(stream, "    m.c[2][2] = axis.c[2]*axis.c[2]*(1-cos(angle)) + cos(angle);\n");
    fprintf(stream, "    m.c[3][3] = 1;\n    return m;\n}\n");

    fprintf(stream, "M4f rotates(float angle, float x, float y, float z)\n{\n");
    fprintf(stream, "    M4f m = m4fs(0.0f);\n");
    fprintf(stream, "    m.c[0][0] = x*x*(1-cos(angle)) + cos(angle);\n");
    fprintf(stream, "    m.c[0][1] = x*y*(1-cos(angle)) - z*sin(angle);\n");
    fprintf(stream, "    m.c[0][2] = x*z*(1-cos(angle)) + y*sin(angle);\n");
    fprintf(stream, "    m.c[1][0] = x*y*(1-cos(angle)) + z*sin(angle);\n");
    fprintf(stream, "    m.c[1][1] = y*y*(1-cos(angle)) + cos(angle);\n");
    fprintf(stream, "    m.c[1][2] = z*y*(1-cos(angle)) - x*sin(angle);\n");
    fprintf(stream, "    m.c[2][0] = x*z*(1-cos(angle)) - y*sin(angle);\n");
    fprintf(stream, "    m.c[2][1] = z*y*(1-cos(angle)) + x*sin(angle);\n");
    fprintf(stream, "    m.c[2][2] = z*z*(1-cos(angle)) + cos(angle);\n");
    fprintf(stream, "    m.c[3][3] = 1;\n    return m;\n}\n");
}


void gen_translation_matrix_impl()
{
}

void gen_transformation_matrix_impl(FILE *stream)
{
    gen_ortho_matrix_impl(stream);
    gen_perspective_matrix_impl(stream);
    gen_rotation_matrix_impl(stream);
}

void gen_matrix_transposition_impl(FILE *stream, size_t n, size_t m, Type_Def type)
{
    fprintf(stream, "%s %s_transpose(%s m)\n{\n",
	    type_mat_name(m, n, type).data,
	    type_mat_prefix(n, m, type).data,
	    type_mat_name(n, m, type).data);
    fprintf(stream, "    %s t;\n", type_mat_name(m, n, type).data);
    fprintf(stream, "    for(int i = 0; i < %zu; ++i)\n", n);
    fprintf(stream, "        for(int j = 0; j < %zu; ++j) t.c[j][i] = m.c[i][j];\n", m);
    fprintf(stream, "    return t;\n}\n");
}
#endif //CLALG_MATRIX_H

#ifndef CLALG_VECTOR_H
#define CLALG_VECTOR_H

#include "common.h"

//     VECTORS DEF
Short_String type_vec_name( size_t n, Type_Def type)
{
    return shortf("V%zu%s", n, type.suffix);
}

Short_String type_vec_prefix( size_t n, Type_Def type)
{
    return shortf("v%zu%s", n, type.suffix);
}

void gen_vector_def(FILE *stream, size_t n, Type_Def type)
{
    fprintf(stream, "typedef struct { %s c[%zu]; } V%zu%s;\n", type.name, n, n, type.suffix);
}

void gen_vector_print_fmt(FILE *stream, size_t n, Type_Def type)
{
    Short_String type_str = type_vec_name(n,type);
    Short_String prefix = type_vec_prefix(n ,type);
    fprintf(stream, "#define %s_fmt \"%s(",type_str.data, prefix.data);
    for(size_t i = 0; i < n; ++i)
    {
	if(i > 0) fprintf(stream, ", ");
	switch(type.suffix[0])
	{
	case 'i':
	    fprintf(stream, "%%d");
	    break;
	case 'u':
	    fprintf(stream, "%%u");
	    break;
	case 's':
	    fprintf(stream, "%%zu");
	    break;
	default:
	    fprintf(stream, "%%f");
	    break;
	}
    }
    fprintf(stream, ")\"\n#define %s_arg(x) ", type_str.data);

    for(size_t i = 0; i < n; ++i)
    {
	if(i > 0) fprintf(stream, ", ");
	fprintf(stream, "x.c[%zu]", i);
    }
    fprintf(stream, "\n\n");
}
//     VECTORS CONSTRUCTOR DECL

void gen_vector_ctor_sig(FILE *stream, size_t n, Type_Def type)
{
    Short_String type_str = type_vec_name(n, type);
    Short_String prefix = type_vec_prefix(n, type);
    fprintf(stream, "%s %s(", type_str.data, prefix.data);
    for(size_t i = 0; i < n; ++i)
    {
	if(i > 0) fprintf(stream, ", ");
	fprintf(stream, "%s x%zu", type.name, i);
    }
    fprintf(stream, ")");
}

void gen_vector_ctor_decl(FILE *stream, size_t n, Type_Def type)
{
    gen_vector_ctor_sig(stream, n, type);
    fprintf(stream, ";\n");
}

void gen_vector_sctor_sig(FILE *stream, size_t n, Type_Def type)
{
    Short_String type_str = type_vec_name(n, type);
    Short_String prefix = type_vec_prefix(n, type);
    fprintf(stream, "%s %ss(%s x)", type_str.data, prefix.data, type.name);
}

void gen_vector_sctor_decl(FILE *stream, size_t n, Type_Def type)
{
    gen_vector_sctor_sig(stream, n, type);
    fprintf(stream, ";\n");
}

//     VECTORS OPERATOR DECL

void gen_vector_op_sig(FILE *stream, size_t n, Type_Def type, Op op)
{
    Short_String type_str = type_vec_name(n, type);
    Short_String f_prefix = type_vec_prefix(n, type);
    fprintf(stream, "%s %s_%s(%s x, %s y)", type_str.data,
	    f_prefix.data, op.suffix,
	    type_str.data, type_str.data);
}

void gen_vector_op_decl(FILE *stream, size_t n, Type_Def type, Op op)
{
    gen_vector_op_sig(stream, n, type, op);
    fprintf(stream, ";\n");
}

void gen_vector_smul_decl(FILE *stream, size_t n, Type_Def type, Type_Def s_type)
{
    Short_String f_prefix = type_vec_prefix(n, type);
    Short_String type_str = type_vec_name(n, type);
    fprintf(stream, "%s %s_smul%s(%s s, %s v);\n",
	    type_str.data, f_prefix.data, s_type.suffix, s_type.name, type_str.data);
}

void gen_scalar_prod_decl(FILE *stream, size_t n, Type_Def type)
{
    Short_String type_str = type_vec_name(n, type);
    Short_String f_prefix = type_vec_prefix(n, type);

    fprintf(stream, "%s %s_scalar(%s x, %s y);\n", type.name, f_prefix.data,
	    type_str.data, type_str.data);
}

void gen_vector_norm_decl(FILE *stream, size_t n, Type_Def type)
{
    Short_String type_str = type_vec_name(n, type);
    Short_String f_prefix = type_vec_prefix(n, type);

    if(strcmp(type.name, "double") != 0)
    {
	fprintf(stream, "float %s_norm(%s v);\n", f_prefix.data, type_str.data);
    }
    else
    {
	fprintf(stream, "double %s_norm(%s v);\n", f_prefix.data, type_str.data);
    }
}

//     VECTORS CONSTRUCTOR IMPL

void gen_vector_ctor_impl(FILE *stream, size_t n, Type_Def type)
{
    Short_String type_str = type_vec_name(n, type);

    gen_vector_ctor_sig(stream, n, type);
    fprintf(stream, "\n{\n    %s v;\n", type_str.data);

    for(size_t i = 0; i < n ; ++i)
	fprintf(stream, "    v.c[%zu] = x%zu;\n", i, i);

    fprintf(stream, "    return v;\n");
    fprintf(stream, "}\n");
}

void gen_vector_sctor_impl(FILE *stream, size_t n, Type_Def type)
{
    Short_String f_prefix = type_vec_prefix(n, type);
    gen_vector_sctor_sig(stream, n, type);
    fprintf(stream, " { return %s(", f_prefix.data);

    for(size_t i = 0; i < n ; ++i)
    {
	if(i > 0) fprintf(stream, ", ");
	fprintf(stream, "x");
    }

    fprintf(stream, "); }\n");
}

//     VECTORS OPERATIONS IMPL

void gen_vector_op_impl(FILE *stream, size_t n, Type_Def type, Op op)
{
    gen_vector_op_sig(stream, n, type, op);
    fprintf(stream, "\n{\n");
    fprintf(stream, "    for(size_t i = 0; i < %zu; ++i)", n);
    fprintf(stream, " x.c[i] %s y.c[i];\n", op.op);
    fprintf(stream, "    return x;\n");
    fprintf(stream, "}\n");
}

void gen_vector_smul_impl(FILE *stream, size_t n, Type_Def type, Type_Def s_type)
{
    Short_String f_prefix = type_vec_prefix(n, type);
    Short_String type_str = type_vec_name(n, type);
    fprintf(stream, "%s %s_smul%s(%s s, %s v)\n{\n",
	    type_str.data, f_prefix.data, s_type.suffix, s_type.name, type_str.data);
    fprintf(stream, "    for(size_t i = 0; i < %zu; ++i)", n);
    fprintf(stream, " v.c[i] *= s;\n    return v;\n}\n");
}

void gen_scalar_prod_impl(FILE *stream, size_t n, Type_Def type)
{
    Short_String type_str = type_vec_name(n, type);
    Short_String f_prefix = type_vec_prefix(n, type);
    fprintf(stream, "%s %s_scalar(%s x, %s y)\n", type.name, f_prefix.data,
	    type_str.data, type_str.data);
    fprintf(stream, "{\n");
    fprintf(stream, "    %s k = 0;\n", type.name);
    fprintf(stream, "    for(size_t i = 0; i < %zu; ++i)", n);
    fprintf(stream, " k += x.c[i] * y.c[i];\n");
    fprintf(stream, "    return k;\n");
    fprintf(stream, "}\n");
}

void gen_vector_norm_impl(FILE *stream, size_t n, Type_Def type)
{
    Short_String type_str = type_vec_name(n, type);
    Short_String f_prefix = type_vec_prefix(n, type);

    if(strcmp(type.name, "double") != 0)
    {
	fprintf(stream, "float %s_norm(%s v)\n{\n", f_prefix.data, type_str.data);
	fprintf(stream, "    %s acc = 0;\n", type.name);
	fprintf(stream, "    for(size_t i = 0; i < %zu; ++i) acc += v.c[i];\n", n);
	fprintf(stream, "    return acc > 0 ? sqrtf(acc) : sqrtf(-acc);\n}\n");
    }
    else
    {
	fprintf(stream, "double %s_norm(%s v)\n{\n", f_prefix.data, type_str.data);
	fprintf(stream, "    %s acc = 0;\n", type.name);
	fprintf(stream, "    for(size_t i = 0; i < %zu; ++i) acc += v.c[i];\n", n);
	fprintf(stream, "    return acc > 0 ? sqrt(acc) : sqrt(-acc);\n}\n");
    }
}

#endif //CLALG_VECTOR_H
